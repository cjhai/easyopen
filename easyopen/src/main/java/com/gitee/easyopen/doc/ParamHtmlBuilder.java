package com.gitee.easyopen.doc;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class ParamHtmlBuilder {
    
    public String buildHtml(ApiDocFieldDefinition definition, String nameVersion) {
        StringBuilder html = new StringBuilder();
        html.append("<tr>")
            .append("<td>"+definition.getName()+"</td>")
            .append("<td>"+definition.getDataType()+"</td>")
            .append("<td>"+this.getRequireHtml(definition)+"</td>")
            .append("<td>"+buildExample(definition, nameVersion, null)+"</td>")
            .append("<td>"+definition.getDescription()+"</td>");
        html.append("</tr>");
        
        return html.toString();
    }
    
    protected String buildExample(ApiDocFieldDefinition definition, String nameVersion, ApiDocFieldDefinition parentDefinition) {
        StringBuilder html = new StringBuilder();
        if(definition.getElements().size() > 0) {
            html.append("<table parentname=\""+ (definition == null ? "" : definition.getName()) +"\">")
                .append("<tr>")
                    .append("<th>名称</th>")
                    .append("<th>类型</th>")
                    .append("<th>是否必须</th>")
                    .append("<th>示例值</th>")
                    .append("<th>描述</th>")
                .append("</tr>");
            
            List<ApiDocFieldDefinition> els = definition.getElements();
            for (ApiDocFieldDefinition apiDocFieldDefinition : els) {
                html.append("<tr>")
                    .append("<td>"+apiDocFieldDefinition.getName()+"</td>")
                    .append("<td>"+apiDocFieldDefinition.getDataType()+"</td>")
                    .append("<td>"+this.getRequireHtml(definition)+"</td>")
                    .append("<td>"+buildExample(apiDocFieldDefinition, nameVersion, definition)+"</td>")
                    .append("<td>"+apiDocFieldDefinition.getDescription()+"</td>")
                .append("</tr>");
            }
            html.append("</table>");
        }else{
            html.append(buildExampleValue(definition, nameVersion, parentDefinition));
        }
        return html.toString();
    }
    
    private String getRequireHtml(ApiDocFieldDefinition definition) {
        if("true".equals(definition.getRequired())) {
            return "<strong>是</strong>";
        }else{
            return "否";
        }
    }

    protected String buildExampleValue(ApiDocFieldDefinition definition, String nameVersion, ApiDocFieldDefinition parentDefinition) {
        String parentname = (parentDefinition == null ? "" : parentDefinition.getName());
        
        String type = "text";
        if(definition.getDataType().equals(DataType.FILE.getValue())
                || definition.getElementClass() == MultipartFile.class) {
            type = "file";
        }
        String id = nameVersion + "_" + definition.getName();
        StringBuilder sb = new StringBuilder();
        
        sb.append("<input id=\"").append(id).append("\" class=\"param-input\" type=\"")
            .append(type).append("\" name=\"").append(definition.getName()).append("\" value=\"")
            .append(definition.getExample()).append("\" ")
            .append(this.getArrAttr(definition))
            .append(this.getDateEvent(definition))
            .append(" parentname=\"").append(parentname).append("\" ")
            .append(" />")
            .append(this.getAddBtn(definition))
            ;
        return sb.toString();
    }
    
    protected String getArrAttr(ApiDocFieldDefinition definition) {
        if(definition.getDataType().equals(DataType.ARRAY.getValue())) {
            return " arrinput=\"true\" ";
        } else {
            return "";
        }
    }

    protected String getDateEvent(ApiDocFieldDefinition definition) {
        if(definition.getDataType().equals(DataType.DATE.getValue())) {
            return " onClick=\"WdatePicker({el:this,dateFmt:'yyyy-MM-dd'})\"";
        } else if(definition.getDataType().equals(DataType.DATETIME.getValue())) {
            return " onClick=\"WdatePicker({el:this,dateFmt:'yyyy-MM-dd HH:mm:ss'})\"";
        }else {
            return "";
        }
    }
    
    protected String getAddBtn(ApiDocFieldDefinition definition) {
        if(definition.getDataType().equals(DataType.ARRAY.getValue())) {
            return " <button type=\"button\" title=\"添加一行\" class=\"add-array-btn\">+</button>" 
                    + " <button type=\"button\" title=\"删除一行\" class=\"del-array-btn\">-</button>";
        } else {
            return "";
        }
    }
}
