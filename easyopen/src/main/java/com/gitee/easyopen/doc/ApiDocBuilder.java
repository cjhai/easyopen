package com.gitee.easyopen.doc;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.annotation.Api;
import com.gitee.easyopen.annotation.ApiService;
import com.gitee.easyopen.doc.annotation.ApiDoc;
import com.gitee.easyopen.doc.annotation.ApiDocBean;
import com.gitee.easyopen.doc.annotation.ApiDocField;
import com.gitee.easyopen.doc.annotation.ApiDocMethod;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 文档生成器
 * 
 * @author tanghc
 *
 */
public class ApiDocBuilder {

    private static String PACKAGE_PREFIX = "java.";

    // key:@ApiDoc.value()
    private Map<String, ApiModule> apiModuleMap = new HashMap<String, ApiModule>(64);

    // key:handler.getClass().getName()
    private static Map<String, ApiDoc> apiDocCache = new HashMap<>(64);

    private ApiConfig apiConfig;

    public ApiDocBuilder(ApiConfig apiConfig) {
        this.apiConfig = apiConfig;
    }

    public Collection<ApiModule> getApiModules() {
        List<ApiModule> apiModules = new ArrayList<>(apiModuleMap.values());

        this.sort(apiModules);

        for (ApiModule apiModule : apiModules) {
            this.sort(apiModule.getModuleItems());
        }

        return apiModules;
    }

    private <T extends Orderable> void sort(List<T> list) {
        List<T> listHasOrder = new ArrayList<>();
        List<T> listNoOrder = new ArrayList<>();

        int max = Integer.MAX_VALUE;

        for (T orderable : list) {
            if (orderable.getOrder() == max) {
                listNoOrder.add(orderable);
            } else {
                listHasOrder.add(orderable);
            }
        }

        Collections.sort(listHasOrder, new Comparator<Orderable>() {
            @Override
            public int compare(Orderable o1, Orderable o2) {
                return Integer.compare(o1.getOrder(), o2.getOrder());
            }
        });

        Collections.sort(listNoOrder, new Comparator<Orderable>() {
            @Override
            public int compare(Orderable o1, Orderable o2) {
                return o1.getOrderName().compareTo(o2.getOrderName());
            }
        });

        list.clear();

        list.addAll(listHasOrder);
        list.addAll(listNoOrder);
    }

    public synchronized void addDocItem(ApiService apiService, Api api, Object handler, Method method) {
        ApiDocMethod apiDocMethod = AnnotationUtils.findAnnotation(method, ApiDocMethod.class);
        if (apiDocMethod == null) {
            return;
        }

        String serviceName = handler.getClass().getSimpleName();
        int order = Integer.MAX_VALUE;

        ApiDoc apiDoc = this.getApiDoc(handler);

        if (apiDoc != null) {
            serviceName = apiDoc.value();
            order = apiDoc.order();
        }

        ApiModule apiModule = this.getApiModule(serviceName, order);

        apiModule.getModuleItems().add(this.buildDocItem(api, apiDocMethod, method));
    }

    private ApiDoc getApiDoc(Object handler) {
        String className = handler.getClass().getName();
        ApiDoc apiDoc = apiDocCache.get(className);
        if (apiDoc == null) {
            apiDoc = AnnotationUtils.findAnnotation(handler.getClass(), ApiDoc.class);
            if (apiDoc != null) {
                apiDocCache.put(className, apiDoc);
            }
        }
        return apiDoc;
    }

    private ApiModule getApiModule(String serviceName, int order) {
        ApiModule apiModule = this.apiModuleMap.get(serviceName);

        if (apiModule == null) {
            apiModule = new ApiModule(serviceName, order);
            this.apiModuleMap.put(serviceName, apiModule);
        }

        return apiModule;
    }

    private ApiDocItem buildDocItem(Api api, ApiDocMethod apiDocMethod, Method method) {
        ApiDocItem docItem = new ApiDocItem();

        String version = api.version();
        if (version == null || "".equals(version)) {
            version = this.apiConfig.getDefaultVersion();
        }

        docItem.setName(api.name());
        docItem.setVersion(version);
        docItem.setDescription(apiDocMethod.description());
        docItem.setRemark(apiDocMethod.remark());
        docItem.setOrder(apiDocMethod.order());

        List<ApiDocFieldDefinition> paramDefinitions = buildParamApiDocFieldDefinitions(apiDocMethod, method);
        List<ApiDocFieldDefinition> resultDefinitions = buildResultApiDocFieldDefinitions(apiDocMethod, method);

        docItem.setParamDefinitions(paramDefinitions);
        docItem.setResultDefinitions(resultDefinitions);

        return docItem;
    }

    private List<ApiDocFieldDefinition> buildParamApiDocFieldDefinitions(ApiDocMethod apiDocMethod, Method method) {
        List<ApiDocFieldDefinition> paramDefinitions = Collections.emptyList();

        ApiDocField[] params = apiDocMethod.params();
        Class<?> paramClass = apiDocMethod.paramClass();
        if (!ArrayUtils.isEmpty(params)) {
            paramDefinitions = buildApiDocFieldDefinitionsByApiDocFields(params);
        } else if (paramClass != Object.class) {
            paramDefinitions = this.buildApiDocFieldDefinitionsByClass(paramClass);
        } else {
            paramDefinitions = this.buildParamDefinitions(method);
        }

        return paramDefinitions;
    }

    private List<ApiDocFieldDefinition> buildResultApiDocFieldDefinitions(ApiDocMethod apiDocMethod, Method method) {
        List<ApiDocFieldDefinition> resultDefinitions = Collections.emptyList();

        Class<?> elClass = apiDocMethod.elementClass();
        if (elClass != Object.class) {
            return buildApiDocFieldDefinitionsByType(elClass);
        }
        ApiDocField[] results = apiDocMethod.results();
        Class<?> resultClass = apiDocMethod.resultClass();
        if (!ArrayUtils.isEmpty(results)) {
            resultDefinitions = buildApiDocFieldDefinitionsByApiDocFields(results);
        } else if (resultClass != Object.class) {
            resultDefinitions = this.buildApiDocFieldDefinitionsByClass(resultClass);
        } else {
            resultDefinitions = this.buildResultDefinitions(method);
        }

        return resultDefinitions;
    }

    private List<ApiDocFieldDefinition> buildApiDocFieldDefinitionsByClass(Class<?> paramClass) {
        ApiDocBean bean = AnnotationUtils.findAnnotation(paramClass, ApiDocBean.class);
        if (bean != null) {
            ApiDocField[] fields = bean.fields();
            if (!ArrayUtils.isEmpty(fields)) {
                return buildApiDocFieldDefinitionsByApiDocFields(fields);
            }
        }

        return buildApiDocFieldDefinitionsByType(paramClass);
    }

    private List<ApiDocFieldDefinition> buildApiDocFieldDefinitionsByApiDocFields(ApiDocField[] params) {
        ArrayList<ApiDocFieldDefinition> paramDefinitions = new ArrayList<ApiDocFieldDefinition>();
        for (ApiDocField apiDocField : params) {
            if (apiDocField.beanClass() != Void.class) {
                paramDefinitions.add(buildApiDocFieldDefinitionByClass(apiDocField, apiDocField.beanClass(), null));
            } else {
                paramDefinitions.add(buildApiDocFieldDefinition(apiDocField, null));
            }
        }
        return paramDefinitions;
    }

    private List<ApiDocFieldDefinition> buildParamDefinitions(Method method) {
        Class<?>[] types = method.getParameterTypes();
        if (types.length == 0) {
            return Collections.emptyList();
        }

        Class<?> paramClass = types[0];

        return buildApiDocFieldDefinitionsByType(paramClass);
    }

    private List<ApiDocFieldDefinition> buildResultDefinitions(Method method) {
        Class<?> type = method.getReturnType();
        if (type == Void.class) {
            return Collections.emptyList();
        }

        return buildApiDocFieldDefinitionsByType(type);
    }

    // 从api参数中构建
    private static List<ApiDocFieldDefinition> buildApiDocFieldDefinitionsByType(Class<?> clazz) {
        if (clazz.isInterface()) {
            return Collections.emptyList();
        }
        final List<String> fieldNameList = new ArrayList<String>();
        final List<ApiDocFieldDefinition> docDefinition = new ArrayList<ApiDocFieldDefinition>();

        // 找到类上面的ApiDocBean注解
        ApiDocBean apiDocBean = AnnotationUtils.findAnnotation(clazz, ApiDocBean.class);
        if (apiDocBean != null) {
            ApiDocField[] fields = apiDocBean.fields();
            for (ApiDocField apiDocField : fields) {
                docDefinition.add(buildApiDocFieldDefinition(apiDocField, null));
                fieldNameList.add(apiDocField.name());
            }
        }
        // 遍历参数对象中的属性
        ReflectionUtils.doWithFields(clazz, new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
                ApiDocField docField = AnnotationUtils.findAnnotation(field, ApiDocField.class);
                if (docField != null) { // 找到有注解的属性
                    ApiDocFieldDefinition fieldDefinition = buildApiDocFieldDefinition(docField, field);
                    Class<?> beanClass = docField.beanClass();
                    Class<?> targetClass = field.getType();

                    if (beanClass != Void.class) {
                        fieldDefinition = buildApiDocFieldDefinitionByClass(docField, beanClass, field);
                    } else if (!isJavaType(targetClass)) { // 如果是自定义类
                        fieldDefinition = buildApiDocFieldDefinitionByClass(docField, targetClass, field);
                    }

                    docDefinition.add(fieldDefinition);
                }
            }
        });

        return docDefinition;
    }

    private static boolean isJavaType(Class<?> type) {
        if (type.isPrimitive()) {
            return true;
        }
        return type.getPackage().getName().startsWith(PACKAGE_PREFIX);
    }

    private static ApiDocFieldDefinition buildApiDocFieldDefinitionByClass(ApiDocField docField, Class<?> clazz,
            Field field) {
        String name = docField.name();
        String type = DataType.OBJECT.getValue();
        String description = docField.description();
        boolean required = docField.required();
        String example = docField.example();

        if (clazz == MultipartFile.class) {
            type = DataType.FILE.getValue();
        }
        if (field != null) {
            name = field.getName();
        }

        ApiDocFieldDefinition fieldDefinition = new ApiDocFieldDefinition();
        fieldDefinition.setName(name);
        fieldDefinition.setDataType(type);
        fieldDefinition.setRequired(String.valueOf(required));
        fieldDefinition.setExample(example);
        fieldDefinition.setDescription(description);

        List<ApiDocFieldDefinition> elementsDefinition = buildApiDocFieldDefinitionsByType(clazz);
        fieldDefinition.setElements(elementsDefinition);

        return fieldDefinition;
    }

    private static ApiDocFieldDefinition buildApiDocFieldDefinition(ApiDocField docField, Field field) {
        String name = docField.name();
        String type = DataType.STRING.getValue();
        DataType dataType = docField.dataType();
        if (dataType == DataType.UNKNOW) {
            type = getFieldType(field);
        } else {
            type = dataType.getValue();
        }
        String description = docField.description();
        boolean required = docField.required();
        String example = docField.example();

        if (field != null) {
            name = field.getName();
        }

        ApiDocFieldDefinition fieldDefinition = new ApiDocFieldDefinition();
        fieldDefinition.setName(name);
        fieldDefinition.setDataType(type);
        fieldDefinition.setRequired(String.valueOf(required));
        fieldDefinition.setExample(example);
        fieldDefinition.setDescription(description);

        List<ApiDocFieldDefinition> elementsDefinition = buildElementListDefinition(docField);
        fieldDefinition.setElements(elementsDefinition);
        fieldDefinition.setElementClass(docField.elementClass());

        if (elementsDefinition.size() > 0) {
            fieldDefinition.setDataType(DataType.ARRAY.getValue());
        }

        return fieldDefinition;
    }

    private static String getFieldType(Field field) {
        if (field == null) {
            return DataType.STRING.getValue();
        }
        Class<?> type = field.getType();
        if (type == List.class || type == Collection.class || type == Set.class) {
            return DataType.ARRAY.getValue();
        }
        if(type == Date.class) {
            return DataType.DATE.getValue();
        }
        if(type == Timestamp.class) {
            return DataType.DATETIME.getValue();
        }
        return field.getType().getSimpleName().toLowerCase();
    }

    private static List<ApiDocFieldDefinition> buildElementListDefinition(ApiDocField docField) {
        Class<?> elClass = docField.elementClass();
        if (elClass != Void.class) {
            return buildApiDocFieldDefinitionsByType(elClass);
        } else {
            return Collections.emptyList();
        }
    }

}
