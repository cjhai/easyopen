package com.gitee.easyopen.bean;

public class Consts {
    
    public static final String NULL = "null";
    public static final String RANDOM_KEY_NAME = "ssl_randomKey";
    public static final String UTF8 = "UTF-8";
    public static final String FORMAT_JSON = "json";
    public static final String FORMAT_XML = "xml";
    public static final String AUTHORIZATION = "Authorization";
    public static final String PREFIX_BEARER = "Bearer ";
    public static final String YMDHMS = "yyyy-MM-dd HH:mm:ss";
    
    public static final String DEFAULT_SIGN_METHOD = "md5";
    
    public static final String CONTENT_TYPE_NAME = "Content-Type";
    public static final String CONTENT_TYPE_JSON = "application/json;charset=UTF-8";
    
    public static final String LINE = "\n";
}
