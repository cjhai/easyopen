package com.gitee.easyopen;

import java.io.Serializable;

public interface Param extends Serializable {

    String fatchName();

    String fatchVersion();

    String fatchAppKey();

    String fatchData();

    String fatchTimestamp();
    
    String fatchSign();
    
    String fatchFormat();
    
    String fatchAccessToken();
    
    String fatchSignMethod();

}