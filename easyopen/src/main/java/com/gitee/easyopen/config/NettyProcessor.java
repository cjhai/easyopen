package com.gitee.easyopen.config;

import io.netty.channel.Channel;

public interface NettyProcessor {
    void process(Channel channel, String data);
}
