package com.gitee.easyopen.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * sha-1加密
 * @author tanghc
 */
public class Sha1Util {

    private static final String ALGORITHM = "SHA1";

    /**
     * sha1加密
     * @param str
     * @return 返回十六进制的字符串形式，全部小写
     */
    public static String encrypt(String str) {
        if (str == null) {
            return null;
        }
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(ALGORITHM);
            messageDigest.update(str.getBytes());
            return HexUtil.byte2hex(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
}
