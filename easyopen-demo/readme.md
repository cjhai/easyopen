# easyopen-demo

四个demo，区别在于配置方式不一样

建议使用easyopen-server-manual，可加深对easyopen的理解。

- easyopen-server-manual:手动配置（springboot）
- easyopen-server-normal:自动配置（springboot&starter）
- easyopen-server-springmvc:手动配置（springmvc）
- easyopen-server-webflux:手动配置，webflux方式（springboot）