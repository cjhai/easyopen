package com.gitee.easyopen.spring.boot.autoconfigure;

import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.config.ConfigClient;
import com.gitee.easyopen.interceptor.ApiInterceptor;
import com.gitee.easyopen.limit.ApiLimitConfigLocalManager;
import com.gitee.easyopen.limit.ApiLimitManager;
import com.gitee.easyopen.support.ApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author tanghc
 */
@org.springframework.context.annotation.Configuration
@EnableConfigurationProperties(EasyopenProperties.class)
public class EasyopenAutoConfiguration {
    Logger logger = LoggerFactory.getLogger(getClass());

    private final EasyopenProperties properties;

    // application.properties中的配置会注入到EasyopenProperties中
    public EasyopenAutoConfiguration(EasyopenProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public ApiConfig fastmybatisConfig() {
        return new ApiConfig();
    }

    /**
     * 默认入口
     */
    @Controller
    @RequestMapping("api")
    public class EayopenIndexController extends ApiController {

        @Autowired
        private ApiConfig config;

        @Autowired(required = false)
        private RedisTemplate redisTemplate;

        @Override
        protected void initApiConfig(ApiConfig apiConfig) {
            BeanUtils.copyProperties(properties, apiConfig);
            if (properties.getAppSecret() != null) {
                apiConfig.addAppSecret(properties.getAppSecret());
            }
            this.initInterceptor(properties, apiConfig);
            this.initOpenClient(properties, apiConfig);
        }

        private void initInterceptor(EasyopenProperties properties, ApiConfig apiConfig) {
            if (!CollectionUtils.isEmpty(properties.getInterceptors())) {
                List<String> interceptors = properties.getInterceptors();
                ApiInterceptor[] apiInterceptor = new ApiInterceptor[interceptors.size()];
                for (int i = 0; i < interceptors.size(); i++) {
                    String interceptorClassName = interceptors.get(i);
                    try {
                        apiInterceptor[i] = (ApiInterceptor)Class.forName(interceptorClassName).newInstance();
                    } catch (Exception e) {
                        logger.error("Class.forName({}).newInstance() error", interceptorClassName, e);
                        throw new RuntimeException(e);
                    }
                }
                apiConfig.setInterceptors(apiInterceptor);
            }
        }

        private void initOpenClient(EasyopenProperties properties, ApiConfig apiConfig) {
            String ip = properties.getConfigServerIp();
            String port = properties.getConfigServerPort();
            if (StringUtils.hasText(ip) && StringUtils.hasText(port)) {
                // appName 应用名称
                // host    配置中心ip
                // port    配置中心端口
                ConfigClient configClient = new ConfigClient(properties.getAppName(), ip, Integer.valueOf(port));
                // 如果要使用分布式业务限流，使用下面这句。默认是单机限流
                if (properties.isConfigDistributedLimit()) {
                    if (redisTemplate == null) {
                        throw new NullPointerException("redisTemplate不能为null，是否缺少spring-boot-starter-data-redis依赖");
                    }
                    configClient.setLimitManager(new ApiLimitManager(redisTemplate, new ApiLimitConfigLocalManager()));
                }
                apiConfig.setConfigClient(configClient);
            }
        }

        @Override
        protected ApiConfig newApiConfig() {
            return config;
        }

    }
}
