/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.5.19 : Database - apiconf
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`apiconf` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `apiconf`;

/*Table structure for table `global_config` */

DROP TABLE IF EXISTS `global_config`;

CREATE TABLE `global_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key_name` varchar(64) NOT NULL,
  `field_name` varchar(64) NOT NULL,
  `field_value` varchar(100) NOT NULL,
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局配置表';

/*Data for the table `global_config` */

/*Table structure for table `limit_app_config` */

DROP TABLE IF EXISTS `limit_app_config`;

CREATE TABLE `limit_app_config` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(100) NOT NULL COMMENT 'app信息',
  `name` varchar(64) NOT NULL,
  `version` varchar(64) NOT NULL,
  `limit_type` varchar(10) NOT NULL COMMENT '限流策略',
  `limit_count` int(11) DEFAULT NULL COMMENT '每秒可处理请求数',
  `limit_code` varchar(20) DEFAULT NULL,
  `limit_msg` varchar(100) DEFAULT NULL,
  `token_bucket_count` int(11) DEFAULT NULL COMMENT '令牌桶容量',
  `gmt_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gmt_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='限流配置';

/*Data for the table `limit_app_config` */

insert  into `limit_app_config`(`id`,`app`,`name`,`version`,`limit_type`,`limit_count`,`limit_code`,`limit_msg`,`token_bucket_count`,`gmt_create`,`gmt_update`) values (1,'app1','hello','','0',1,'222','333',4,'2018-07-03 15:46:11','2018-07-04 12:32:09'),(2,'app1','hello2','','0',2,'333','4444',5,'2018-07-03 15:46:29','2018-07-03 15:46:13');

/*Table structure for table `perm_api_info` */

DROP TABLE IF EXISTS `perm_api_info`;

CREATE TABLE `perm_api_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '0' COMMENT '接口名',
  `version` varchar(50) NOT NULL COMMENT '版本号',
  `app` varchar(50) NOT NULL COMMENT '所属app',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0：使用中，1：未使用',
  `gmt_create` datetime NOT NULL,
  `gmt_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=581 DEFAULT CHARSET=utf8 COMMENT='接口信息表';

/*Data for the table `perm_api_info` */

insert  into `perm_api_info`(`id`,`name`,`version`,`app`,`status`,`gmt_create`,`gmt_update`) values (548,'manager.session.get','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(549,'goods.add','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(550,'doc.param.6','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(551,'param.type.3','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(552,'doc.param.5','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(553,'param.type.4','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(554,'doc.param.4','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(555,'doc.param.3','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(556,'doc.param.2','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(557,'doc.param.1','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(558,'wrapResult.false','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(559,'session.set','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(560,'doc.result.7','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(561,'doc.result.4','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(562,'doc.result.3','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(563,'goods.pageinfo','2.0','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(564,'doc.result.6','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(565,'doc.result.5','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(566,'doc.result.2','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(567,'doc.result.1','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(568,'goods.get','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(569,'hello','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(570,'user.goods.get','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(571,'userjwt.goods.get','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(572,'file.upload2','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(573,'goods.list','2.0','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(574,'goods.pageinfo','1.0','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(575,'file.upload3','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(576,'file.upload','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(577,'session.get','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(578,'param.type.1','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(579,'param.type.2','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59'),(580,'userlock.test','','app1',0,'2018-07-09 09:43:59','2018-07-09 09:43:59');

/*Table structure for table `perm_client` */

DROP TABLE IF EXISTS `perm_client`;

CREATE TABLE `perm_client` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `app_key` varchar(100) NOT NULL COMMENT 'appKey',
  `secret` varchar(200) NOT NULL COMMENT 'secret',
  `pub_key` text COMMENT '公钥',
  `pri_key` text COMMENT '私钥',
  `app` varchar(50) NOT NULL COMMENT 'app名称',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '0启用，1禁用',
  `gmt_create` datetime NOT NULL,
  `gmt_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_app_key` (`app_key`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='app信息表';

/*Data for the table `perm_client` */

insert  into `perm_client`(`id`,`app_key`,`secret`,`pub_key`,`pri_key`,`app`,`status`,`gmt_create`,`gmt_update`) values (1,'admin','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 16:25:20'),(2,'test1','1234561','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 16:25:11'),(3,'aaaa','123456',NULL,NULL,'app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(4,'bbbb','123456',NULL,NULL,'app1',0,'2018-01-22 21:03:17','2018-07-09 14:24:23'),(5,'cccc','123456',NULL,NULL,'app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(7,'ddd','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(8,'eeee','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(9,'ffff','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(10,'ggggg','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(11,'hhhhhh','123456','MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDhlWrRQXJeI09CyCB/L2Yxcbh2IMMSxwB+V99Y\r\nt+ZWeVhcZUPRRcM79ThLSEpkd9QLX/A+ZleI1K9RssbJhxZ7t9XuJNXgZlBzIF5yVmgZl7bRR767\r\n+XZxnf8jm7KZ2rSdVwyCGvl+1CBlEYLRnv9sB1ZpkzBCj12gBinYMj5xZQIDAQAB','MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOGVatFBcl4jT0LIIH8vZjFxuHYg\r\nwxLHAH5X31i35lZ5WFxlQ9FFwzv1OEtISmR31Atf8D5mV4jUr1GyxsmHFnu31e4k1eBmUHMgXnJW\r\naBmXttFHvrv5dnGd/yObspnatJ1XDIIa+X7UIGURgtGe/2wHVmmTMEKPXaAGKdgyPnFlAgMBAAEC\r\ngYEAsbfUSn0kC/QHapZdu7Vs7kEoULAo3u82fVLfG3buGWxJ56jDz+gFEoRzUCPor9QTks6HZ7Ga\r\n/qqIYHXW1Ef/tgdUUeldjrKtmpc9H/U8+KqiryXasu9FpCTlMM4T/mZoowhJPpkG/7jNsoNizHN7\r\nXN1d3RQBdLr72Ip+U8Git2ECQQD4vuaLccLWjkTlFzLaV3wfrUCsyAkaRKCmGFNMq7a5ubN4QtTi\r\nfPSQdmoDGVD1F9Q+nBtXeSWbt047qvaqfII5AkEA6CmXm8YPh0xYntn94UxO31saw729P+hXuzNv\r\nabusSJOdrA2x1Jqz01AKjOpohUENl/6NyfXPxjRRCSEI46B4jQJAbOxbVBCauw1Nievgrs/EYLKj\r\nMYXexovqtRDN2TMQLr/soOrTAeKpzWCtB3JcixbGMCx3pJQ+LbPVJDe3D+y5sQJAAe1WdNSQDG91\r\nzNvCX7xiazg2YKmSiJVFJSioJBiKtY+EH4l9kGY4V+iyLblEZNbFZh2Wz7ZaoyqMAadki38pgQJA\r\nDVXVMV5X+vlfi338IGefp2DdROSPJr8vusp6b/VKEJIxxzESIQHqg8NkJmf8gj6sy3ijlR8p9Wrk\r\nfh5WjsD43w==','app1',0,'2018-01-22 21:03:17','2018-07-09 09:33:15'),(12,'iiiii','123456','','','app1',0,'2018-01-22 21:03:17','2018-07-09 16:32:57'),(13,'adm','123','','','app1',0,'2018-07-09 14:00:40','2018-07-09 16:32:40'),(14,'123','123','','','app1',0,'2018-07-09 15:53:54','2018-07-09 16:32:30');

/*Table structure for table `perm_client_role` */

DROP TABLE IF EXISTS `perm_client_role`;

CREATE TABLE `perm_client_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) NOT NULL COMMENT '客户端id',
  `role_code` varchar(50) NOT NULL COMMENT '角色code',
  `gmt_create` datetime NOT NULL,
  `gmt_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_role` (`client_id`,`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='客户端角色';

/*Data for the table `perm_client_role` */

insert  into `perm_client_role`(`id`,`client_id`,`role_code`,`gmt_create`,`gmt_update`) values (7,2,'normal','2018-07-09 16:25:12','2018-07-09 16:25:12'),(9,1,'pay','2018-07-09 16:25:20','2018-07-09 16:25:20'),(10,14,'normal','2018-07-09 16:32:30','2018-07-09 16:32:30'),(11,13,'normal','2018-07-09 16:32:40','2018-07-09 16:32:40'),(12,12,'normal','2018-07-09 16:32:57','2018-07-09 16:32:57');

/*Table structure for table `perm_role` */

DROP TABLE IF EXISTS `perm_role`;

CREATE TABLE `perm_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) NOT NULL COMMENT '角色代码',
  `description` varchar(50) NOT NULL COMMENT '角色描述',
  `gmt_create` datetime NOT NULL,
  `gmt_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_code` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `perm_role` */

insert  into `perm_role`(`id`,`role_code`,`description`,`gmt_create`,`gmt_update`) values (1,'normal','普通ISV','2018-01-23 09:21:59','2018-01-23 09:22:02'),(2,'pay','付费ISV','2018-07-09 15:04:26','2018-07-09 15:04:27');

/*Table structure for table `perm_role_permission` */

DROP TABLE IF EXISTS `perm_role_permission`;

CREATE TABLE `perm_role_permission` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) NOT NULL COMMENT '角色表code',
  `api_id` bigint(20) NOT NULL COMMENT 'api_id',
  `gmt_create` datetime NOT NULL,
  `gmt_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_perm` (`role_code`,`api_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色权限表';

/*Data for the table `perm_role_permission` */

insert  into `perm_role_permission`(`id`,`role_code`,`api_id`,`gmt_create`,`gmt_update`) values (1,'normal',1,'2018-01-23 09:39:00','2018-01-23 09:39:02'),(2,'normal',2,'2018-01-23 09:39:00','2018-01-23 09:39:02');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
