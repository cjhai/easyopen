var Main = (function () {

    var $tabs = $("#mainTab");
    var $tabsMenu = $("#tabsMenu");
    //绑定tabs的右键菜单
    $tabs.tabs({
        onContextMenu: function (e, title) {
            e.preventDefault();
            $('#tabsMenu').menu('show', {
                left: e.pageX,
                top: e.pageY
            }).data("tabTitle", title);
        }
    });

    //实例化menu的onClick事件
    $tabsMenu.menu({
        onClick: function (item) {
            closeTab(this, item.name);
        }
    });

    //几个关闭事件的实现
    function closeTab(menu, type) {
        var curTabTitle = $(menu).data("tabTitle");
        if (type === "close") {
            doCloseTab(curTabTitle);
        } else if (type === "Refresh") {
            var $curTab = $tabs.tabs('getSelected'); // 获得当前tab
            var url = $($curTab.panel('options').content).attr('src');
            if (url) {
                $tabs.tabs('update', {
                    tab: $curTab,
                    options: {
                        content: '<iframe src="'+url+'?app=' + curTabTitle + '&q='+new Date().getTime()+'" scrolling="yes" frameborder="0" style="width:100%;height:99%;"></iframe>'
                    }
                });
            }
        } else {
            var allTabs = $tabs.tabs("tabs");
            var closeTabsTitle = [];

            $.each(allTabs, function () {
                var opt = $(this).panel("options");
                if (opt.closable && opt.title != curTabTitle && type === "Other") {
                    closeTabsTitle.push(opt.title);
                } else if (opt.closable && type === "All") {
                    closeTabsTitle.push(opt.title);
                }
            });

            for (var i = 0; i < closeTabsTitle.length; i++) {
                $tabs.tabs("close", closeTabsTitle[i]);
            }
        }
    }

    function doCloseTab(title) {
        $tabs.tabs("close", title);
    }

    //在右边center区域打开菜单，新增tab
    function openTab(node) {
        var text = node.text;
        if ($tabs.tabs('exists', text)) {
            $tabs.tabs('select', text);
        } else {
            $tabs.tabs('add', {
                title: text,
                closable: true,
                content: '<iframe src="config.html?app=' + text + '&q='+new Date().getTime()+'" scrolling="yes" frameborder="0" style="width:100%;height:99%;"></iframe>'
            });
        }
    }

    $('#appTree').tree({
        loader: function (param, success, error) {
            ApiUtil.post('app.list', param, function (resp) {
                var respData = resp.data;
                success(respData);
            });
        }
        , onClick: function (node) {
            if (!node.root) {
                openTab(node);
            }
        }
    });

    $('#sysMenu').tree({
       onClick: function (node) {
           var text = node.text;
           if ($tabs.tabs('exists', text)) {
               $tabs.tabs('select', text);
           } else {
               var url = node.url;
               if(url) {
                   $tabs.tabs('add', {
                       title: text,
                       closable: true,
                       content: '<iframe src="'+url+'?q='+new Date().getTime()+'" scrolling="yes" frameborder="0" style="width:100%;height:99%;"></iframe>'
                   });
               }
           }
        }
    });

    // public函数
    return {

    }// end return;
})();