package com.gitee.apiconf.common;

import com.gitee.apiconf.entity.AdminUser;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class WebContext {
    private static WebContext INSTANCE = new WebContext();

    private static final String S_USER = "s_user";

    private WebContext() {
    }

    public static WebContext getInstance() {
        return INSTANCE;
    }

    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 等同session.setAttribute(key, value);
     *
     * @param key
     * @param value
     */
    public void setSessionAttr(String key, Object value) {
        this.getSession().setAttribute(key, value);
    }

    /**
     * 等同session.getAttribute(key);
     *
     * @param key
     * @return
     */
    public Object getSessionAttr(String key) {
        return this.getSession().getAttribute(key);
    }

    public HttpSession getSession() {
        if (getRequest() != null) {
            return getRequest().getSession();
        }
        return null;
    }

    /**
     * 获取当前登录用户
     *
     * @return
     */
    public AdminUser getLoginUser() {
        return (AdminUser) this.getSession().getAttribute(S_USER);
    }


    /**
     * 保存当前用户
     *
     * @param session
     * @param user
     */
    public void setLoginUser(HttpSession session, AdminUser user) {
        this.getSession().setAttribute(S_USER, user);
    }

}
