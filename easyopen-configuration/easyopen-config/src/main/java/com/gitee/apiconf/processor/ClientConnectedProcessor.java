package com.gitee.apiconf.processor;

import com.gitee.apiconf.common.ChannelContext;
import com.gitee.easyopen.config.ConfigMsg;

import io.netty.channel.Channel;

public class ClientConnectedProcessor extends AbstractNettyServerProcessor {
    @Override
    public void process(Channel channel, ConfigMsg msg) {
        ChannelContext.saveChannel(msg.getApp(), channel);
    }
}
